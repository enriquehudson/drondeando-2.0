import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons/faArrowLeft';
import {faClose} from '@fortawesome/free-solid-svg-icons/faClose';

import DrawerView from '../components/DrawerView/DrawerView';
import TabNavigator from './TabNavigator';
import Detail from '../components/Detail';
import Contact from '../components/Contact';
import About from '../components/About';

const DrawerNavigator = () => {
  const Drawer = createDrawerNavigator();

  const backButton = (navigation: any) => (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <FontAwesomeIcon icon={faClose} size={20} />
    </TouchableOpacity>
  );

  return (
    <Drawer.Navigator drawerContent={DrawerView}>
      <Drawer.Screen name="Drondeando" component={TabNavigator} />
      <Drawer.Screen
        name="Detail"
        component={Detail}
        options={({navigation}) => ({
          title: '',
          headerLeft: () => (
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => navigation.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} size={16} />

              <Text style={styles.textBack}>Regresar</Text>
            </TouchableOpacity>
          ),
          headerLeftContainerStyle: styles.leftContainer,
        })}
      />
      <Drawer.Screen
        name="Contact"
        component={Contact}
        options={({navigation}) => ({
          headerLeft: () => null,
          title: 'Contacto',
          headerRight: () => backButton(navigation),
          headerRightContainerStyle: styles.rightContainer,
        })}
      />
      <Drawer.Screen
        name="About"
        component={About}
        options={({navigation}) => ({
          headerLeft: () => null,
          title: 'Sobre nosotros',
          headerRight: () => backButton(navigation),
          headerRightContainerStyle: styles.rightContainer,
        })}
      />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textBack: {
    marginLeft: 4,
  },
  leftContainer: {
    paddingLeft: 5,
  },
  rightContainer: {
    paddingRight: 5,
  },
});

export default DrawerNavigator;
