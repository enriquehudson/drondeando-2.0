import React, {useEffect, useState} from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import TabList from '../components/TabList';
import useGetPost from '../hooks/useGetPosts';
import ViewMap from '../components/ViewMap';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faImage} from '@fortawesome/free-solid-svg-icons/faImage';
import {faTableCells} from '@fortawesome/free-solid-svg-icons/faTableCells';
import {faMap} from '@fortawesome/free-solid-svg-icons/faMap';
import TabBoxes from '../components/TabBoxes';

const TabNavigator = () => {
  const Tab = createBottomTabNavigator();
  const [isFirstLoad, setIsFirstLoad] = useState(true);
  const {getPosts} = useGetPost();

  useEffect(() => {
    if (isFirstLoad) {
      getPosts();
      setIsFirstLoad(false);
    }
  }, [getPosts, isFirstLoad, setIsFirstLoad]);

  return (
    <Tab.Navigator screenOptions={{tabBarShowLabel: false, headerShown: false}}>
      <Tab.Screen
        name="List"
        component={TabList}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color}) => (
            <FontAwesomeIcon icon={faImage} color={color} size={18} />
          ),
        }}
      />

      <Tab.Screen
        name="Box"
        component={TabBoxes}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color}) => (
            <FontAwesomeIcon icon={faTableCells} color={color} size={18} />
          ),
        }}
      />

      <Tab.Screen
        name="Mapa"
        component={ViewMap}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesomeIcon icon={faMap} color={color} size={18} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
