import {useContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {API} from '../services/APIConnection';
import {Post} from '../commons/types';
import {WordPress} from '../commons/WordPressType';
import {AppContext} from '../context/AppContext';
import {setPosts} from '../context/appCreators';

const useGetPosts = () => {
  const {appState, appDispatch} = useContext(AppContext);

  const getPosts = async (
    offset: number = 0,
    per_page: number = 8,
    exclude: number[] = [],
  ) => {
    console.log('antes', appState.posts);
    const storedPosts = await AsyncStorage.getItem('posts');
    let posts: Post[] = storedPosts ? JSON.parse(storedPosts) : [];
    if (posts.length > 0) {
      appDispatch(setPosts(posts));
    }

    const response = await API.get('posts', {
      params: {
        order: 'desc',
        per_page,
        offset,
        'filter[meta_key]': 'content',
        'filter[meta_compare]': '<>',
        'filter[meta_value]': '',
        exclude,
      },
    });
    console.log(response.headers);
    console.log('total ' + response.headers['x-wp-total']);
    console.log('totalPages ' + response.headers['x-wp-totalpages']);
    response.data
      .filter((item: WordPress) => item.acf.image)
      .map((item: WordPress) => {
        if (!posts.some(post => post.key === item.id)) {
          console.log('agregando');
          const newPost: Post = {
            key: item.id,
            title: item.title.rendered,
            type: item.acf.type,
            content: item.acf.content,
            coordinate: {
              latitude:
                typeof item.acf.location.lat === 'string'
                  ? parseFloat(item.acf.location.lat)
                  : item.acf.location.lat,
              longitude:
                typeof item.acf.location.lng === 'string'
                  ? parseFloat(item.acf.location.lng)
                  : item.acf.location.lng,
            },
            place: item.acf.place,
            imageName: item.acf.image,
            instagram: item.acf.instagram || '',
            twitter: item.acf.twitter || '',
            facebook: item.acf.facebook || '',
            youTube: item.acf.youtube || '',
          };
          posts.push(newPost);
        } else {
          console.log('ya existe');
        }
      });

    posts = posts.sort((a: Post, b: Post) => b.key - a.key);

    appDispatch(setPosts(posts));
    AsyncStorage.setItem('posts', JSON.stringify(posts));
  };

  return {getPosts};
};

export default useGetPosts;
