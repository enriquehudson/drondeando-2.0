export interface Post {
  key: number;
  title: string;
  type: string;
  content: string;
  coordinate: Coordinate;
  place: string;
  imageName: string;
  instagram: string;
  facebook: string;
  twitter: string;
  youTube: string;
}

export type AppState = {
  posts: Post[];
};

export interface Coordinate {
  latitude: number;
  longitude: number;
}
export interface Store {
  Posts: Post[];
}
