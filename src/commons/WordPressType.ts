export interface WordPress {
  id: number;
  date: Date;
  date_gmt: Date;
  guid: GUID;
  modified: Date;
  modified_gmt: Date;
  slug: string;
  status: string;
  type: string;
  link: string;
  title: GUID;
  content: Content;
  excerpt: Content;
  author: number;
  featured_media: number;
  comment_status: string;
  ping_status: string;
  sticky: boolean;
  template: string;
  format: string;
  meta: Meta;
  categories: number[];
  tags: any[];
  acf: Acf;
  yoast_head: string;
  yoast_head_json: YoastHeadJSON;
  jetpack_featured_media_url: string;
  jetpack_publicize_connections: any[];
  jetpack_sharing_enabled: boolean;
  jetpack_shortlink: string;
  _links: Links;
}

export interface Links {
  self: About[];
  collection: About[];
  about: About[];
  author: AuthorElement[];
  replies: AuthorElement[];
  'version-history': VersionHistory[];
  'predecessor-version': PredecessorVersion[];
  'wp:featuredmedia': AuthorElement[];
  'wp:attachment': About[];
  'wp:term': WpTerm[];
  curies: Cury[];
}

export interface About {
  href: string;
}

export interface AuthorElement {
  embeddable: boolean;
  href: string;
}

export interface Cury {
  name: string;
  href: string;
  templated: boolean;
}

export interface PredecessorVersion {
  id: number;
  href: string;
}

export interface VersionHistory {
  count: number;
  href: string;
}

export interface WpTerm {
  taxonomy: string;
  embeddable: boolean;
  href: string;
}

export interface Acf {
  instagram: string;
  twitter: string;
  facebook: string;
  youtube: string;
  type: string;
  image: string;
  place: string;
  content: string;
  location: Location;
}

export interface Location {
  address: string;
  lat: number;
  lng: number;
  zoom: number;
  place_id: string;
  street_number: string;
  street_name: string;
  street_name_short: string;
  city: string;
  state: string;
  state_short: string;
  post_code: string;
  country: string;
  country_short: string;
}

export interface Content {
  rendered: string;
  protected: boolean;
}

export interface GUID {
  rendered: string;
}

export interface Meta {
  _mi_skip_tracking: boolean;
  spay_email: string;
  jetpack_publicize_message: string;
  jetpack_is_tweetstorm: boolean;
  jetpack_publicize_feature_enabled: boolean;
}

export interface YoastHeadJSON {
  title: string;
  description: string;
  robots: Robots;
  canonical: string;
  og_locale: string;
  og_type: string;
  og_title: string;
  og_description: string;
  og_url: string;
  og_site_name: string;
  article_publisher: string;
  article_published_time: Date;
  article_modified_time: Date;
  og_image: OgImage[];
  twitter_card: string;
  twitter_creator: string;
  twitter_site: string;
  twitter_misc: TwitterMisc;
  schema: Schema;
}

export interface OgImage {
  width: number;
  height: number;
  url: string;
  type: string;
}

export interface Robots {
  index: string;
  follow: string;
  'max-snippet': string;
  'max-image-preview': string;
  'max-video-preview': string;
}

export interface Schema {
  '@context': string;
  '@graph': Graph[];
}

export interface Graph {
  '@type': string;
  '@id': string;
  url?: string;
  name?: string;
  description?: string;
  potentialAction?: PotentialAction[];
  inLanguage?: string;
  contentUrl?: string;
  width?: number;
  height?: number;
  caption?: string;
  isPartOf?: BreadcrumbClass;
  primaryImageOfPage?: BreadcrumbClass;
  datePublished?: Date;
  dateModified?: Date;
  author?: BreadcrumbClass;
  breadcrumb?: BreadcrumbClass;
  itemListElement?: ItemListElement[];
  image?: Image;
}

export interface BreadcrumbClass {
  '@id': string;
}

export interface Image {
  '@type': string;
  '@id': string;
  inLanguage: string;
  url: string;
  contentUrl: string;
  caption: string;
}

export interface ItemListElement {
  '@type': string;
  position: number;
  name: string;
  item?: string;
}

export interface PotentialAction {
  '@type': string;
  target: string[] | TargetClass;
  'query-input'?: string;
}

export interface TargetClass {
  '@type': string;
  urlTemplate: string;
}

export interface TwitterMisc {
  'Escrito por': string;
  'Tiempo de lectura': string;
}
