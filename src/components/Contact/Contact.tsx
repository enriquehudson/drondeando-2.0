import React, {useState} from 'react';
import {View, Alert, Image, Text, TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import styles from './Contact.styles';

const Contact = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const sendMail = () => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let error = '';
    if (message === '') {
      error = 'Capture Mensaje';
    }

    if (reg.test(email) === false) {
      error = 'Correo Invalido';
    }

    if (email === '') {
      error = 'Capture correo';
    }

    if (name === '') {
      error = 'Capture su nombre';
    }

    if (error) {
      Alert.alert('Datos Faltantes', error);
    } else {
      const formData = new FormData();
      formData.append('name', name);
      formData.append('email', email);
      formData.append('textarea', message);
      fetch('https://drondeando.com/app/sendmail.php', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData,
      })
        .then(serviceResponse => {
          return serviceResponse.json();
        })
        .then(response => {
          if (response.status === 'success') {
            setName('');
            setEmail('');
            setMessage('');
            Alert.alert('Correo enviado', 'Gracias por tus comentarios');
          } else {
            console.log(response);
            Alert.alert('Error', 'Error enviando correo intente más tarde');
          }
        })
        .catch(err => {
          console.warn('fetch error:', err);
        });
    }
  };

  return (
    <View>
      <Image
        source={require('../../../assets/banner2.jpeg')}
        style={styles.imageHeader}
      />
      <Text style={styles.instructions}>
        Envía tus comentarios o sugerencias
      </Text>
      <Text style={styles.label}>Nombre:</Text>

      <TextInput
        style={styles.input}
        value={name}
        placeholder="Nombre"
        onChangeText={setName}
      />
      <Text style={styles.label}>Correo:</Text>
      <TextInput
        style={styles.input}
        value={email}
        placeholder="Correo"
        onChangeText={setEmail}
        autoCapitalize="none"
      />
      <Text style={styles.label}>Comentario:</Text>
      <TextInput
        multiline
        numberOfLines={5}
        onChangeText={setMessage}
        value={message}
        style={styles.textarea}
      />
      <TouchableOpacity onPress={sendMail} style={styles.button}>
        <Text style={styles.textButton}>Enviar</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Contact;
