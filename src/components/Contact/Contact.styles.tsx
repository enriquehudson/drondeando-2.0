import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  imageHeader: {
    height: 100,
    width: Dimensions.get('window').width,
  },
  instructions: {
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
    marginBottom: 10,
    marginTop: 5,
  },
  label: {
    fontSize: 14,
    color: '#333',
    paddingLeft: 12,
  },
  input: {
    height: 40,
    marginHorizontal: 12,
    marginBottom: 12,
    marginTop: 5,
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 6,
  },
  textarea: {
    height: 100,
    marginHorizontal: 12,
    marginTop: 5,
    marginBottom: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 6,
  },
  button: {
    backgroundColor: '#335EFF',
    width: 120,
    height: 40,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  textButton: {
    fontSize: 16,
    color: '#fff',
  },
});

export default styles;
