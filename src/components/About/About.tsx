import React from 'react';
import {Image, Linking, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faYoutube} from '@fortawesome/free-brands-svg-icons/faYoutube';
import {faInstagram} from '@fortawesome/free-brands-svg-icons/faInstagram';
import {faTwitter} from '@fortawesome/free-brands-svg-icons/faTwitter';
import {faFacebook} from '@fortawesome/free-brands-svg-icons/faFacebook';
import {faLaptop} from '@fortawesome/free-solid-svg-icons/faLaptop';

import styles from './About.style';

const About = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.imageHeader}
        source={require('../../../assets/banner1.jpeg')}
      />
      <View style={styles.textView}>
        <Text>
          Drondeando es un proyecto que pretende mostrar diferentes fotografías
          tomadas con ayuda de drones como una manera de dar a conocer lugares
          desde otro punto de vista.
        </Text>
      </View>
      <Text style={styles.title1}>Siguenos en redes sociales</Text>
      <View style={styles.socialNetworks}>
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            Linking.openURL('https://www.facebook.com/Drondeando/')
          }>
          <FontAwesomeIcon
            size={23}
            icon={faFacebook}
            color="#3B5998"
            style={styles.icon}
          />
          <Text style={styles.textLinks}>facebook.com/drondeando</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            Linking.openURL('https://www.twitter.com/drondeando/')
          }>
          <FontAwesomeIcon
            size={23}
            icon={faTwitter}
            color="#00aced"
            style={styles.icon}
          />
          <Text style={styles.textLinks}>@Drondeando</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            Linking.openURL('https://www.instagram.com/drondeando_mx/')
          }>
          <FontAwesomeIcon
            size={23}
            icon={faInstagram}
            color="#e95950"
            style={styles.icon}
          />
          <Text style={styles.textLinks}>@Drondeando_mx</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            Linking.openURL(
              'https://www.youtube.com/channel/UCYdkiGM_mD30BC_rNBqxeQg',
            )
          }>
          <FontAwesomeIcon
            size={20}
            icon={faYoutube}
            color="red"
            style={styles.icon}
          />
          <Text style={styles.textLinks}>youtube.com/drondeando</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => Linking.openURL('https://www.drondeando.com/')}>
          <FontAwesomeIcon
            size={23}
            icon={faLaptop}
            color="black"
            style={styles.icon}
          />
          <Text style={styles.textLinks}>www.drondeando.com</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.versionContainer}>
        <Text style={styles.textVersion}>Version: 2.0</Text>
      </View>
    </View>
  );
};

export default About;
