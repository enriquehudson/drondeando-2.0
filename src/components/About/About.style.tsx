import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eee',
  },
  title1: {
    margin: 7,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  socialNetworks: {
    marginRight: 50,
    marginLeft: 45,
    justifyContent: 'space-between',
    textAlign: 'right',
  },
  textView: {
    margin: 10,
    marginBottom: 25,
  },
  imageHeader: {
    height: 200,
    width: Dimensions.get('window').width,
  },
  viewImageHeader: {
    height: 250,
  },
  versionContainer: {
    marginTop: 50,
    marginBottom: 30,
    marginRight: 20,
  },
  textVersion: {
    textAlign: 'right',
  },
  textLinks: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
  },
  button: {
    flexDirection: 'row',
    width: '100%',
    padding: 1,
    alignItems: 'center',
  },
  icon: {
    padding: 10,
    minWidth: 45,
  },
});

export default styles;
