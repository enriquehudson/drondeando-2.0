import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  view: {
    flex: 1,
  },
  img: {
    flex: 1,
    margin: 5,
    height: 150,
  },
  viewImg: {
    height: 150,
  },
  map: {
    height: '100%',
  },
});

export default styles;
