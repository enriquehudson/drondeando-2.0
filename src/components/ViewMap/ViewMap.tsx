import React, {useContext} from 'react';
import MapView, {Marker} from 'react-native-maps';
import {View} from 'react-native';

import {AppContext} from '../../context/AppContext';
import {Post} from '../../commons/types';
import styles from './ViewMap.style';

const ViewMap = ({navigation}: any) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const {appState, appDispatch} = useContext(AppContext);

  return (
    <View style={styles.view}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 18.906272,
          longitude: -101.939746,
          latitudeDelta: 15.0,
          longitudeDelta: 15.0,
        }}>
        {appState.posts.map(
          (marker: Post) =>
            marker.coordinate.latitude && (
              <Marker
                key={marker.key}
                coordinate={marker.coordinate}
                title={marker.title}
                description={marker.content}
                onPress={e => {
                  e.stopPropagation();
                  navigation.navigate('Detail', {item: marker});
                }}
              />
            ),
        )}
      </MapView>
    </View>
  );
};

export default ViewMap;
