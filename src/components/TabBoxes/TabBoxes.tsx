import React, {useContext, useState} from 'react';
import {SafeAreaView, FlatList, Image, RefreshControl} from 'react-native';
import useGetPosts from '../../hooks/useGetPosts';

import {AppContext} from '../../context/AppContext';

import {Post} from '../../commons/types';
import styles from './TabBoxes.style';
import BoxElement from '../BoxElement';

const TabBoxes = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const {appState, appDispatch} = useContext(AppContext);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const {getPosts} = useGetPosts();

  const onRefresh = React.useCallback(async () => {
    setIsRefreshing(true);
    getPosts(0, 2).then(() => setIsRefreshing(false));
  }, [getPosts]);

  const loadMore = () => {
    const ids: number[] = appState.posts.map((item: Post) => item.key);
    getPosts(0, 8, ids);
  };

  const renderItem = ({item}: {item: Post}) => <BoxElement item={item} />;
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <Image
        style={styles.headerImage}
        source={require('../../../assets/banner3.jpeg')}
      />
      <FlatList
        data={appState.posts}
        numColumns={3}
        renderItem={renderItem}
        keyExtractor={item => item.key.toString()}
        onEndReached={loadMore}
        onEndReachedThreshold={1}
        refreshControl={
          <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
        }
      />
    </SafeAreaView>
  );
};

export default TabBoxes;
