import {StyleSheet, Platform, StatusBar, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  headerImage: {
    width: Dimensions.get('window').width,
    marginVertical: 5,
    height: 100,
  },
});

export default styles;
