import React from 'react';
import {Text, View, SafeAreaView, Image} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {WebView} from 'react-native-webview';
import MapView, {Marker} from 'react-native-maps';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faGlobe} from '@fortawesome/free-solid-svg-icons/faGlobe';
import {Post} from '../../commons/types';
import ButtonSocial from '../ButtonSocial';
import styles from './Detail.style';

const Detail = ({route}: any) => {
  const {item}: {item: Post} = route.params;

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <ScrollView style={styles.scroll}>
        <Text style={styles.title}>{item.title}</Text>
        {item.youTube !== '' && (
          <WebView
            useWebKit={true}
            style={styles.webView}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            source={{
              uri:
                'https://www.youtube.com/embed/' +
                item.youTube +
                '?rel=0&autoplay=0&showinfo=0&controls=0',
            }}
          />
        )}
        <Image
          source={{uri: item.imageName}}
          resizeMode="cover"
          style={styles.image}
        />
        <Text style={styles.content}>{item.content}</Text>
        <View style={styles.viewPlace}>
          <FontAwesomeIcon icon={faGlobe} size={16} color="#000" />
          <Text style={styles.place}>{item.place}</Text>
        </View>
        {item.facebook !== '' && item.instagram !== '' && item.twitter !== '' && (
          <>
            <Text style={styles.textNetworks}>Ver redes sociales</Text>
            <View style={styles.socialNetworks}>
              {item.facebook && (
                <ButtonSocial url={item.facebook} type="facebook" />
              )}
              {item.instagram && (
                <ButtonSocial url={item.instagram} type="instagram" />
              )}
              {item.twitter && (
                <ButtonSocial url={item.twitter} type="twitter" />
              )}
              {item.youTube !== '' && (
                <ButtonSocial url={item.youTube} type="youtube" />
              )}
            </View>
          </>
        )}
        {item.coordinate.latitude && (
          <MapView
            style={styles.map}
            region={{
              latitude: item.coordinate.latitude,
              longitude: item.coordinate.longitude,
              latitudeDelta: 0.00625,
              longitudeDelta: 0.00625,
            }}>
            <Marker
              coordinate={item.coordinate}
              title={item.title}
              description={item.content}
            />
          </MapView>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Detail;
