import {Dimensions, Platform, StatusBar, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  header: {
    backgroundColor: 'lightblue',
    height: 30,
    justifyContent: 'center',
    paddingLeft: 5,
  },
  scroll: {
    paddingHorizontal: 5,
  },
  title: {
    fontSize: 22,
    textAlign: 'center',
    padding: 5,
  },
  image: {
    width: Dimensions.get('screen').width - 10,
    height: Dimensions.get('screen').width - 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
  },
  webView: {
    marginBottom: 10,
    width: Dimensions.get('window').width - 10,
    height: Dimensions.get('window').width / 1.5,
  },
  content: {
    fontSize: 15,
    marginTop: 5,
    textAlign: 'justify',
  },
  viewPlace: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  place: {
    marginLeft: 5,
  },
  textNetworks: {
    marginTop: 10,
    textAlign: 'center',
    backgroundColor: '#ccc',
    borderTopRightRadius: 6,
    borderTopLeftRadius: 6,
  },
  socialNetworks: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 10,
    padding: 2,
    backgroundColor: '#eee',
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  map: {
    width: Dimensions.get('screen').width - 10,
    height: Dimensions.get('screen').width / 2.5,
  },
});

export default styles;
