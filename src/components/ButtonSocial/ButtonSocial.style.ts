import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  facebook: {
    color: '#3B5998',
  },
  twitter: {
    color: '#00aced',
  },
  instagram: {
    color: '#e95950',
  },
  youtube: {
    color: 'red',
  },
});

export default styles;
