import React from 'react';
import {Linking} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faYoutube} from '@fortawesome/free-brands-svg-icons/faYoutube';
import {faInstagram} from '@fortawesome/free-brands-svg-icons/faInstagram';
import {faTwitter} from '@fortawesome/free-brands-svg-icons/faTwitter';
import {faFacebook} from '@fortawesome/free-brands-svg-icons/faFacebook';

import styles from './ButtonSocial.style';

type ButtonSocialProps = {
  type: 'instagram' | 'facebook' | 'twitter' | 'youtube';
  url: string;
};

const ButtonSocial = ({type, url}: ButtonSocialProps) => {
  let icon;
  switch (type) {
    case 'instagram':
      icon = faInstagram;
      break;
    case 'facebook':
      icon = faFacebook;
      break;
    case 'twitter':
      icon = faTwitter;
      break;
    case 'youtube':
      icon = faYoutube;
      break;
  }

  return (
    <TouchableOpacity onPress={() => Linking.openURL(url)}>
      <FontAwesomeIcon icon={icon} style={styles[type]} size={25} />
    </TouchableOpacity>
  );
};

export default ButtonSocial;
