import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderColor: '#999',
    borderWidth: 1,
    margin: 8,
    paddingVertical: 5,
    height: 260,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  titleIcon: {
    marginRight: 3,
  },
  title: {
    flex: 1,
  },
  image: {
    width: '100%',
    height: 200,
    borderColor: '#333',
    borderWidth: 1,
  },
  rowTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 4,
    paddingHorizontal: 4,
  },
});

export default styles;
