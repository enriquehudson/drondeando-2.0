import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {useNavigation} from '@react-navigation/native';

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faImage} from '@fortawesome/free-solid-svg-icons/faImage';
import {faYoutube} from '@fortawesome/free-brands-svg-icons/faYoutube';
import {faMap} from '@fortawesome/free-solid-svg-icons/faMap';
import {Post} from '../../commons/types';

import styles from '../ListElement/ListElement.style';

const ListElement = ({item}: {item: Post}) => {
  const navigation = useNavigation<DrawerNavigationProp<any>>();
  let icon = faImage;
  let iconSize = 24;

  if (item.type === 'video') {
    icon = faYoutube;
    iconSize = 20;
    console.log(item.title, item.youTube);
  }

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Detail', {item})}>
      <View style={styles.rowTitle}>
        <FontAwesomeIcon
          icon={icon}
          color="#333"
          size={iconSize}
          style={styles.titleIcon}
        />

        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          {item.title}
        </Text>
      </View>
      <Image
        source={{uri: item.imageName}}
        resizeMode="cover"
        style={styles.image}
      />

      <View style={styles.row}>
        <Text>{item.place}</Text>
        {item.coordinate && (
          <FontAwesomeIcon icon={faMap} color="#666" size={18} />
        )}
      </View>
    </TouchableOpacity>
  );
};

export default ListElement;
