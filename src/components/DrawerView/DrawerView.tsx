import React from 'react';

import {View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faInfo} from '@fortawesome/free-solid-svg-icons/faInfo';
import {faEnvelope} from '@fortawesome/free-solid-svg-icons/faEnvelope';
import styles from './DrawerView.style';

const DrawerView = ({navigation}: any) => {
  return (
    <View style={styles.container}>
      <View style={styles.viewTop} />
      <View style={styles.viewLogo}>
        <Image
          source={require('../../../assets/image1.jpeg')}
          style={styles.imageLogo}
        />
        <Text style={styles.title}>Drondeando</Text>
      </View>
      <View>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => navigation.navigate('Contact')}>
          <View style={styles.iconView}>
            <FontAwesomeIcon icon={faEnvelope} size={18} color="#fff" />
          </View>
          <Text style={styles.textButton}>Contacto</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttons}
          onPress={() => navigation.navigate('About')}>
          <View style={styles.iconView}>
            <FontAwesomeIcon icon={faInfo} size={20} color="#fff" />
          </View>
          <Text style={styles.textButton}>Acerca de</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DrawerView;
