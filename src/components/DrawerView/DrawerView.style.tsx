import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#999',
  },
  viewTop: {
    height: 30,
    backgroundColor: '#4c91ff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewLogo: {
    height: 270,
    backgroundColor: '#4c91ff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttons: {
    marginBottom: 0,
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#335EFF',
    alignItems: 'center',
  },
  iconView: {
    padding: 2,
    minWidth: 28,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    color: '#fff',
    fontSize: 15,
    marginLeft: 5,
  },
  imageLogo: {
    height: 150,
    width: 150,
    borderRadius: 75,
  },
  title: {
    margin: 10,
    textAlign: 'center',
    fontSize: 26,
  },
});

export default styles;
