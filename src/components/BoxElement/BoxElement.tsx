import React from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {useNavigation} from '@react-navigation/core';

import {Post} from '../../commons/types';
import styles from './BoxElement.style';

const BoxElement = ({item}: {item: Post}) => {
  const navigation = useNavigation<DrawerNavigationProp<any>>();
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Detail', {item})}>
      <Image
        resizeMode="cover"
        style={styles.image}
        source={{uri: item.imageName}}
      />
    </TouchableOpacity>
  );
};

export default BoxElement;
