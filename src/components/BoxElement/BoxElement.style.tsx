import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {},
  image: {
    width: Dimensions.get('window').width / 3,
    height: Dimensions.get('window').width / 3,
    borderColor: '#fff',
    borderWidth: 1,
  },
});

export default styles;
