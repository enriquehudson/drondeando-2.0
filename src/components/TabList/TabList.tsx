import React, {useContext, useState} from 'react';
import {SafeAreaView, FlatList, RefreshControl, Button} from 'react-native';

import {AppContext} from '../../context/AppContext';
import ListElement from '../ListElement';
import {Post} from '../../commons/types';
import styles from './TabList.style';
import AsyncStorage from '@react-native-async-storage/async-storage';
import useGetPosts from '../../hooks/useGetPosts';

const TabList = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const {appState, appDispatch} = useContext(AppContext);
  const [isRefreshing, setIsRefreshing] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [isScrolling, setIsScrolling] = useState(false);

  const {getPosts} = useGetPosts();

  const onRefresh = React.useCallback(async () => {
    setIsRefreshing(true);
    getPosts(0, 2).then(() => setIsRefreshing(false));
  }, [getPosts]);

  const loadMore = () => {
    const ids: number[] = appState.posts.map((item: Post) => item.key);
    getPosts(0, 8, ids);
  };

  const renderItem = ({item}: {item: Post}) => <ListElement item={item} />;

  const borrar = async () => {
    await AsyncStorage.clear();
  };

  const ver = () => {
    const ids: number[] = appState.posts.map((item: Post) => item.key);
    console.log(ids);
  };

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      {/* <Button onPress={borrar} title="Borrar" />
      <Button onPress={ver} title="Ver" /> */}
      <FlatList
        data={appState.posts}
        renderItem={renderItem}
        keyExtractor={item => item.key.toString()}
        onMomentumScrollBegin={() => setIsScrolling(true)}
        onMomentumScrollEnd={() => setIsScrolling(false)}
        onEndReached={loadMore}
        onEndReachedThreshold={1}
        refreshControl={
          <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
        }
      />
    </SafeAreaView>
  );
};

export default TabList;
