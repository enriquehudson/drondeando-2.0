import {GET_COMMENTS, GET_POSTS, CLEAN_FLAGS} from './actionsTypes';
import {Post} from '../commons/types';

export const readComments = () => {
  return {
    type: GET_COMMENTS,
  };
};

export const setPosts = (posts: Post[]) => {
  return {
    type: GET_POSTS,
    payload: posts,
  };
};

export const cleanFlags = () => {
  return {
    type: CLEAN_FLAGS,
  };
};
