import {AppState} from '../commons/types';
import {CLEAN_FLAGS, GET_COMMENTS, GET_POSTS} from './actionsTypes';

export const appInitialState: AppState = {
  posts: [],
};

const AppReducer = (state: any = appInitialState, action: any): any => {
  switch (action.type) {
    case GET_POSTS:
      return {...state, posts: action.payload};
    case GET_COMMENTS:
      return {...state};
    case CLEAN_FLAGS:
      return {
        appInitialState,
      };
  }
};

export default AppReducer;
