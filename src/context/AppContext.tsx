import React from 'react';
import {AppState} from '../commons/types';

import {appInitialState} from './AppReducer';

export const AppContext = React.createContext<{
  appState: AppState;
  appDispatch: React.Dispatch<any>;
}>({
  appState: appInitialState,
  appDispatch: () => undefined,
});

export default AppContext;
