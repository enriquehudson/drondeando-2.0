import React, {useReducer} from 'react';
import AppContext from './AppContext';
import AppReducer, {appInitialState} from './AppReducer';

const AppProvider = (props: any) => {
  const [appState, appDispatch] = useReducer(AppReducer, appInitialState);

  const appContext = {
    appState,
    appDispatch,
  };

  return (
    <AppContext.Provider value={appContext}>
      {props.children}
    </AppContext.Provider>
  );
};

export default AppProvider;
