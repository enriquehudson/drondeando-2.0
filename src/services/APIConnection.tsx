import axios from 'axios';

const baseUrl = 'https://drondeando.com/wp-json/wp/v2/';

export const API = axios.create({
  baseURL: baseUrl,
  timeout: 8000,
  headers: {'Content-Type': 'application/json'},
});
