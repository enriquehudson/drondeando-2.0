/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RNBootSplash from 'react-native-bootsplash';

import DrawerNavigator from './src/navigators/DrawerNavigator';
import AppProvider from './src/context/appProvider';
const App = () => {
  useEffect(() => {
    const init = async () => {
      await RNBootSplash.hide({fade: true});
    };
    init();
  }, []);

  return (
    <AppProvider>
      <NavigationContainer>
        <DrawerNavigator />
      </NavigationContainer>
    </AppProvider>
  );
};

export default App;
